# SNAPINSTALL

Esto es un script sencillo para instalar el servicio snap en las siguientes distribuciones de GNU/Linux:
<p></p>

<ul>
  <li>Arch Linux</li>
  <li>Debian</li>
  <li>Fedora</li>
  <li>Kali Linux</li>
  <li>Kubuntu</li>
  <li>Lubuntu</li>
  <li>OpenSUSE Thunbleweed</li>
  <li>PopOs</li>
  <li>Xubuntu</li>
  <li>Elementary Os</li>
  <li>KDE Neón</li>
  <li>Linux Mint</li>
  <li>Manjaro</li>
  <li>Ubuntu</li>
  <li>Zorin Os</li>
</ul>


# FUNCIONAMIENTO/INSTALACIÓN:

El script se ejecuta con permisos de ejecución en una terminal y el resto sera indicar la distribución que tienes, abajo te indico las ordenes necesarias que tienes que ejecutar.


```bash
chmod +x snapinstall.sh    #para dar permisos de ejecución al script
```

```bash
sudo ./snapinstall.sh     #Para ejecutar el script
```
